import java.math.BigInteger;

/**
 * 11/apr/2019
 * <p>
 * Lattice paths
 * Starting in the top left corner of a 2×2 grid, and only being able to move to the right and down, there are exactly 6 routes to the bottom right corner.
 * How many such routes are there through a 20×20 grid?
 */
public class Problem15 {

    public static void main(String[] args) {
        /*
         * (n)         n!
         * ( ) = ---------------
         * (k)    k! * (n - k)!
         */
        int x = 20;
        int y = 20;

        long startTime = Z.now();

        int n = x + y;
        int k = x;

        BigInteger nf = Z.factorial(n);
        BigInteger kf = Z.factorial(k);

        BigInteger possiblePaths = nf.divide(kf.multiply(Z.factorial(n - k)));

        Z.printAnswer(possiblePaths.toString(), startTime);
    }
}
