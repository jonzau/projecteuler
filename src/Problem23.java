import java.util.ArrayList;

/**
 * 9/jul/2020
 * <p>
 * A perfect number is a number for which the sum of its proper divisors is exactly equal to the number. For example, the sum of the proper divisors of 28 would be 1 + 2 + 4 + 7 + 14 = 28, which means that 28 is a perfect number.
 * <p>
 * A number n is called deficient if the sum of its proper divisors is less than n and it is called abundant if this sum exceeds n.
 * <p>
 * As 12 is the smallest abundant number, 1 + 2 + 3 + 4 + 6 = 16, the smallest number that can be written as the sum of two abundant numbers is 24. By mathematical analysis, it can be shown that all integers greater than 28123 can be written as the sum of two abundant numbers. However, this upper limit cannot be reduced any further by analysis even though it is known that the greatest number that cannot be expressed as the sum of two abundant numbers is less than this limit.
 * <p>
 * Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers.
 */
public class Problem23 {

    private static int minAbundant = 12;

    private static int MAX = 28123;

    private static boolean isAbundant(int num) {
        if (num < minAbundant || num > MAX)
            return false;
        long sum = 0;
        Long[] divisors = Z.getDivisors(num, true);
        for (int i = divisors.length - 1; i >= 0; i--) {
            long d = divisors[i];
            sum += d;
            if (sum > num)
                return true;
        }
        return false;
    }

    private static boolean canBeWrittenAsSumOf2Abundants(int num, ArrayList<Integer> abundantList) {
        for (int n : abundantList) {
            if (n > num / 2)
                return false;
            int remain = num - n;
            if (abundantList.contains(remain))
                return true;
        }
        return false;
    }

    public static void main(String[] args) {
        long startTime = Z.now();
        ArrayList<Integer> abundantList = new ArrayList<>();
        for (int i = minAbundant; i <= MAX; i++) {
            if (isAbundant(i))
                abundantList.add(i);
        }

        ArrayList<Integer> asSumList = new ArrayList<>();
        for (int i = 1; i < MAX; i++) {
            if (!canBeWrittenAsSumOf2Abundants(i, abundantList))
                asSumList.add(i);
        }

        long sum = 0;
        for (int n : asSumList) {
            sum += n;
        }

        Z.printAnswer(sum, startTime);
    }
}
