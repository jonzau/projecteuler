import java.math.BigInteger;

/**
 * 12/apr/2019
 * <p>
 * n! means n × (n − 1) × ... × 3 × 2 × 1
 * <p>
 * For example, 10! = 10 × 9 × ... × 3 × 2 × 1 = 3628800,
 * and the sum of the digits in the number 10! is 3 + 6 + 2 + 8 + 8 + 0 + 0 = 27.
 * <p>
 * Find the sum of the digits in the number 100!
 */
public class Problem20 {

    public static void main(String[] args) {
        long startTime = Z.now();
        BigInteger hundredFactorial = Z.factorial(100);

        long sum = 0;
        for (char c : hundredFactorial.toString().toCharArray()) {
            sum += Integer.parseInt("" + c);
        }

        Z.printAnswer(sum, startTime);
    }
}
