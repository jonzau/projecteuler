import java.util.ArrayList;

/**
 * 12/apr/2019
 * <p>
 * By starting at the top of the triangle below and moving to adjacent numbers on the row below, the maximum total from top to bottom is 23.
 * <p>
 * 3
 * 7 4
 * 2 4 6
 * 8 5 9 3
 * <p>
 * That is, 3 + 7 + 4 + 9 = 23.
 * <p>
 * Find the maximum total from top to bottom of the triangle below:
 * <p>
 * 75
 * 95 64
 * 17 47 82
 * 18 35 87 10
 * 20 04 82 47 65
 * 19 01 23 75 03 34
 * 88 02 77 73 07 63 67
 * 99 65 04 28 06 16 70 92
 * 41 41 26 56 83 40 80 70 33
 * 41 48 72 33 47 32 37 16 94 29
 * 53 71 44 65 25 43 91 52 97 51 14
 * 70 11 33 28 77 73 17 78 39 68 17 57
 * 91 71 52 38 17 14 91 43 58 50 27 29 48
 * 63 66 04 68 89 53 67 30 73 16 69 87 40 31
 * 04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
 * <p>
 * NOTE: As there are only 16384 routes, it is possible to solve this problem by trying every route. However, Problem 67, is the same challenge with a triangle containing one-hundred rows; it cannot be solved by brute force, and requires a clever method! ;o)
 */
public class Problem18 {

    static ArrayList<Long> sums = new ArrayList<>();

    public static class Item {
        public int value;
        public Item left = null;
        public Item right = null;

        public Item(int value) {
            this.value = value;
        }

        public Item(String valueStr) {
            this(Integer.parseInt(valueStr));
        }

        public void add(long sum) {
            sum += value;
            if (left == null)
                sums.add(sum);
            else {
                left.add(sum);
                right.add(sum);
            }
        }
    }

    public static void main(String[] args) {
        long startTime = Z.now();

        String input = "75\n" +
                "95 64\n" +
                "17 47 82\n" +
                "18 35 87 10\n" +
                "20 04 82 47 65\n" +
                "19 01 23 75 03 34\n" +
                "88 02 77 73 07 63 67\n" +
                "99 65 04 28 06 16 70 92\n" +
                "41 41 26 56 83 40 80 70 33\n" +
                "41 48 72 33 47 32 37 16 94 29\n" +
                "53 71 44 65 25 43 91 52 97 51 14\n" +
                "70 11 33 28 77 73 17 78 39 68 17 57\n" +
                "91 71 52 38 17 14 91 43 58 50 27 29 48\n" +
                "63 66 04 68 89 53 67 30 73 16 69 87 40 31\n" +
                "04 62 98 27 23 09 70 98 73 93 38 53 60 04 23";

        String[] lines = input.split("\n");
        Item root = null;
        Item[] itemsHere = null;
        for (int i = 0; i < lines.length - 1; i++) {
            if (root == null) {
                root = new Item(lines[0]);
                itemsHere = new Item[]{root};
            }
            String[] linesBelow = lines[i + 1].split(" ");

            Item[] itemsBelow = new Item[linesBelow.length];

            for (int j = 0; j < linesBelow.length; j++) {
                itemsBelow[j] = new Item(linesBelow[j]);
            }

            for (int j = 0; j < itemsHere.length; j++) {
                Item currentItem = itemsHere[j];
                currentItem.left = itemsBelow[j];
                currentItem.right = itemsBelow[j + 1];
            }

            itemsHere = itemsBelow;
        }

        root.add(0);

        long largestSum = 0;
        for (long sum : sums) {
            if (sum > largestSum)
                largestSum = sum;
        }

        Z.printAnswer(largestSum, startTime);
    }
}
