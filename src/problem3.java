import java.util.Date;

/**
 * The prime factors of 13195 are 5, 7, 13 and 29.
 * What is the largest prime factor of the number 600851475143 ?
 */
public class problem3 {

    public static void main(String[] args) {
        long num = 600851475143L;
        long ans = 0;
        long mul = 0;

        long f;
        double d;

        long startTime = new Date().getTime();
        System.out.println("Searching for largest prime multiple of " + num + " ...");

        for (long i = 2; i < num / 2 + 1; i++) {
            if (i % 10000000 == 0) {
                //print i and time elapsed every 10,000,000. just to see progress
                System.out.println((1.0 * (new Date().getTime() - startTime) / 1000) + " sec --- " + mul + " multiples --- " + i);
            }

            f = num / i;
            //the next 4 lines are to make sure that num / i is not a fraction
            d = 1.0 * num / i;
            if (d - f != 0) {
                continue;
            }

            //if i is multiple of num
            if (num % f == 0) {
                mul++;
                boolean isPrime = true;
                //check if prime
                for (long j = 2; j < f; j++) {
                    if (f % j == 0) {
                        isPrime = false;
                        break;
                    }
                }
                if (isPrime) {
                    ans = f;
                    break;
                }
            }

        }

        System.out.println("The answer is: " + ans);
        System.out.println(mul + " multiples were checked.");
        System.out.println("Calculated in: " + (1.0 * (new Date().getTime() - startTime) / 1000) + " sec");
    }
}
