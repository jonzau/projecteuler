import java.util.Date;

/**
 * The sum of the primes below 10 is 2 + 3 + 5 + 7 = 17.
 * Find the sum of all the primes below two million.
 */
public class problem10 {

    public static void main(String[] args) {
        long ans = 0;
        int max = 2000000;
        long startTime = new Date().getTime();
        int[] primes = new int[max];
        int index = 0;

        for (int i = 2; i < max; i++) {
            if (Z.isPrime(i)) {
                ans = ans + i;
                //primes[index] = i;
                index++;
            }
        }


        long tt = new Date().getTime() - startTime;
        double t = 1.0 * tt / 1000;
        System.out.println("Answer: " + ans);
        System.out.println("in " + t + " sec");
        System.out.println("Index: " + index);
    }
}
