import java.math.BigInteger;
import java.util.ArrayList;

/**
 * Commonly used utilities.
 */
public class Z {

    public static boolean isEven(final long num) {
        return num % 2 == 0;
    }

    public static boolean isPrime(final long num) {
        if (num < 2) {
            return false;
        } else if (num <= 3) {
            return true;
        } else if (num % 2 == 0 || num % 3 == 0) {
            return false;
        }
        for (long i = 5; i * i <= num; i++) {
            if (num % i == 0) {
                return false;
            }
        }
        return true;
    }

    public static boolean isDivisor(final long divisor, final long num) {
        return divisor > 0 && num % divisor == 0;
    }

    public static Long[] getDivisors(final long num, final boolean excludeNumber) {
        ArrayList<Long> divisors = new ArrayList<>();
        for (long i = 1; i <= num / 2; i++) {
            if (isDivisor(i, num)) {
                divisors.add(i);
            }
        }
        if (!excludeNumber) {
            divisors.add(num);
        }
        return divisors.toArray(new Long[divisors.size()]);
    }

    public static int getDivisorsNumber(final long num) {
        if (num == 1) {
            return 1;
        }

        long limit = num;
        int divisors = 0;
        for (int i = 1; i < limit; ++i) {
            if (num % i == 0) {
                limit = num / i;
                if (limit != i) {
                    divisors++;
                }
                divisors++;
            }
        }

        return divisors;
    }

    public static BigInteger factorial(final long num) {
        if (num < 1)
            return null;
        if (num == 1)
            return new BigInteger("1");
        return new BigInteger("" + num).multiply(Z.factorial(num - 1));
    }

    public static long now() {
        return System.currentTimeMillis();
    }

    public static double time(final long startTime) {
        long tt = now() - startTime;
        return (1.0 * tt / 1000);
    }

    public static void printAnswer(final long ans, final long startTime) {
        printAnswer("" + ans, startTime);
    }

    public static void printAnswer(final String ans, final long startTime) {
        System.out.println("Answer: " + ans);
        System.out.println("in " + time(startTime) + " sec");
    }

    public static void main(String[] args) {
        long[] numbers = new long[]{16111, 16112};
        for (long x : numbers) {
            if (Z.isPrime(x)) {
                System.out.println("" + x + " is prime");
            } else {
                System.out.println("" + x + " is not prime");
            }
        }

        long startTime = now() - (3 * 1000 + 213);
        printAnswer(123, startTime);

        int num = 5;
        for (int i = 0; i <= num; i++) {
            System.out.println(i + ": " + isDivisor(i, num));
        }

        for (long div : getDivisors(num, false)) {
            System.out.println(div);
        }
        System.out.println(getDivisorsNumber(num));

        for (int i = 0; i < 10; i++) {
            System.out.println(i + " - " + isEven(i));
        }

        System.out.println("5! = " + Z.factorial(5));
        System.out.println("40! = " + Z.factorial(40));
    }
}
