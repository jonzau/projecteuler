import java.util.Date;

import static java.lang.Math.pow;

/*
 * A Pythagorean triplet is a set of three natural numbers, a < b < c, for which,
 * a2 + b2 = c2
 * For example, 32 + 42 = 9 + 16 = 25 = 52.
 * There exists exactly one Pythagorean triplet for which a + b + c = 1000.
 * Find the product abc.
 */
public class problem9 {
    
    public static void main(String[] args) {
        int a = 0;
        int b = 0;
        int c = 0;
        int ans = 0;
        long startTime = new Date().getTime();

        for (int i = 1; i < 1000000; i++) {
            long c2;
            for (int j = i + 1; j < 1000000; j++) {
                c2 = (long) (pow(i, 2) + pow(j, 2));
                double cd = Math.sqrt(c2);
                if (i + j + cd > 1000) {
                    break;
                }
                if (cd % 1 == 0) {
                    c = (int) cd;
                    if (i + j + c == 1000) {
                        a = i;
                        b = j;
                        ans = a * b * c;
                    }
                }

            }

        }

        long tt = new Date().getTime() - startTime;
        double t = 1.0 * tt / 1000;
        System.out.println("Answer: " + ans);
        System.out.println("in " + t + " sec");
        System.out.println("a: " + a);
        System.out.println("b: " + b);
        System.out.println("c: " + c);
    }
}
