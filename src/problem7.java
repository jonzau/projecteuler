import java.util.Date;

/**
 * By listing the first six prime numbers: 2, 3, 5, 7, 11, and 13, we can see that the 6th prime is 13.
 * What is the 10 001st prime number?
 */
public class problem7 {

    public static void main(String[] args) {
        int num = 0;
        int count = 0;
        boolean isPrime;

        long startTime = new Date().getTime();

        for (int i = 2; i < 1000000000; i++) {
            isPrime = true;
            for (int j = 2; j < i; j++) {
                if (i % j == 0) {
                    isPrime = false;
                    break;
                }
            }
            if (isPrime) {
                num = i;
                count++;
                if (count == 10001) {
                    break;
                }
            }
        }


        long tt = new Date().getTime() - startTime;
        double t = 1.0 * tt / 1000;
        System.out.println("Answer: " + num);
        System.out.println("in " + t + " sec");
    }
}
