/**
 * 5/jul/2018
 *
 * The following iterative sequence is defined for the set of positive integers:
 * n → n/2 (n is even)
 * n → 3n + 1 (n is odd)
 * Using the rule above and starting with 13, we generate the following sequence:
 * 13 → 40 → 20 → 10 → 5 → 16 → 8 → 4 → 2 → 1
 * It can be seen that this sequence (starting at 13 and finishing at 1) contains 10 terms. Although it has not been proved yet (Collatz Problem), it is thought that all starting numbers finish at 1.
 * Which starting number, under one million, produces the longest chain?
 * NOTE: Once the chain starts the terms are allowed to go above one million.
 */
public class problem14 {

    public static long next(final long n) {
        if (Z.isEven(n)) {
            return n / 2;
        } else {
            return 3 * n + 1;
        }
    }

    public static void main(String[] args) {
        final int maxNum = 1000000;
        long maxCount = 0;
        int maxN = 0;
        final long startTime = Z.now();
        for (int i = 1; i <= maxNum; i++) {
            long count = 1;
            long n = i;
            while (n != 1) {
                count++;
                n = next(n);
            }
            if (count > maxCount) {
                maxCount = count;
                maxN = i;
            }
        }

        Z.printAnswer(maxN, startTime);
    }
}
