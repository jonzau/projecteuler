/**
 * 12/apr/2019
 * <p>
 * If the numbers 1 to 5 are written out in words: one, two, three, four, five, then there are 3 + 3 + 5 + 4 + 4 = 19 letters used in total.
 * If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
 * NOTE: Do not count spaces or hyphens. For example, 342 (three hundred and forty-two) contains 23 letters and 115 (one hundred and fifteen) contains 20 letters. The use of "and" when writing out numbers is in compliance with British usage.
 */
public class Problem17 {

    private static String getUnidad(int num) {
        String one = "one";
        String two = "two";
        String three = "three";
        String four = "four";
        String five = "five";
        String six = "six";
        String seven = "seven";
        String eight = "eight";
        String nine = "nine";

        String[] numbers = new String[]{one, two, three, four, five, six, seven, eight, nine};

        if (num > 0 && num <= numbers.length)
            return numbers[num - 1];
        return "";
    }

    private static String get10To19(int num) {
        String ten = "ten";
        String eleven = "eleven";
        String twelve = "twelve";
        String thirteen = "thirteen";
        String fourteen = "fourteen";
        String fifteen = "fifteen";
        String sixteen = "sixteen";
        String seventeen = "seventeen";
        String eighteen = "eighteen";
        String nineteen = "nineteen";

        String[] numbers = new String[]{ten, eleven, twelve, thirteen,
                fourteen, fifteen, sixteen, seventeen, eighteen, nineteen};

        if (num >= 0 && num < 10)
            return numbers[num];
        return "";
    }

    private static String getDecena(int num, boolean includeDash) {
        String twenty = "twenty";
        String thirty = "thirty";
        String forty = "forty";
        String fifty = "fifty";
        String sixty = "sixty";
        String seventy = "seventy";
        String eighty = "eighty";
        String ninety = "ninety";

        String[] numbers = new String[]{twenty, thirty, forty, fifty, sixty, seventy, eighty, ninety};
        if (num > 1 && num < 10)
            return numbers[num - 2] + (includeDash ? "-" : "");
        return "";
    }

    private static String getCentena(int num, boolean inlcudeAnd) {
        if (num > 0 && num < 10)
            return getUnidad(num) + " hundred " + (inlcudeAnd ? "and " : "");
        return "";
    }

    private static String getCUnidadDeMil(int num) {
        if (num > 0 && num < 10)
            return getUnidad(num) + " thousand ";
        return "";
    }

    private static String getNumberals(int num) {
        char[] numStr = ("" + num).toCharArray();

        int unidad = -1;
        int decena = -1;
        int centena = -1;
        int unidadDeMil = -1;

        if (numStr.length > 0)
            unidad = Integer.parseInt("" + numStr[numStr.length - 1]);
        if (numStr.length > 1)
            decena = Integer.parseInt("" + numStr[numStr.length - 2]);
        if (numStr.length > 2)
            centena = Integer.parseInt("" + numStr[numStr.length - 3]);
        if (numStr.length > 3)
            unidadDeMil = Integer.parseInt("" + numStr[numStr.length - 4]);

        String numeral = "";
        if (unidadDeMil > -1)
            numeral += getCUnidadDeMil(unidadDeMil);
        if (centena > -1)
            numeral += getCentena(centena, decena > 0 || unidad > 0);
        if (decena > -1) {
            if (decena == 1 && unidad > -1)
                numeral += get10To19(unidad);
            else
                numeral += getDecena(decena, unidad > 0);
        }
        if (unidad > -1 && decena != 1)
            numeral += getUnidad(unidad);

        return numeral;
    }

    public static void main(String[] args) {
        long startTime = Z.now();
        int start = 1;
        int end = 1000;
        String allNums = "";
        for (int i = start; i <= end; i++) {
            allNums += getNumberals(i) + "\n";
        }

        allNums = allNums.replaceAll("\n", "");
        allNums = allNums.replaceAll("-", "");
        allNums = allNums.replaceAll(" ", "");

        Z.printAnswer(allNums.length(), startTime);
    }
}
