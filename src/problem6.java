import java.util.Date;

/*
 * The sum of the squares of the first ten natural numbers is,
 * 12 + 22 + ... + 102 = 385
 * The square of the sum of the first ten natural numbers is,
 * (1 + 2 + ... + 10)2 = 552 = 3025
 * Hence the difference between the sum of the squares of the first ten natural numbers and the square of the sum is 3025 − 385 = 2640.
 * Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
 */
public class problem6 {
    
    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 0;

        long startTime = new Date().getTime();

        for (int i = 1; i < 101; i++) {
            n1 = (int) (n1 + Math.pow(i, 2));
        }

        for (int i = 1; i < 101; i++) {
            n2 = n2 + i;
        }
        n2 = n2 * n2;

        int ans = n2 - n1;
        long tt = new Date().getTime() - startTime;
        double t = 1.0 * tt / 1000;
        System.out.println("Answer: " + ans);
        System.out.println("in " + t + " sec");

    }
}
