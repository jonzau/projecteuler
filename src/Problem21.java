import java.util.ArrayList;
import java.util.HashSet;

/**
 * 12/apr/2019
 * <p>
 * Let d(n) be defined as the sum of proper divisors of n (numbers less than n which divide evenly into n).
 * If d(a) = b and d(b) = a, where a ≠ b, then a and b are an amicable pair and each of a and b are called amicable numbers.
 * <p>
 * For example, the proper divisors of 220 are 1, 2, 4, 5, 10, 11, 20, 22, 44, 55 and 110; therefore d(220) = 284. The proper divisors of 284 are 1, 2, 4, 71 and 142; so d(284) = 220.
 * <p>
 * Evaluate the sum of all the amicable numbers under 10000.
 */
public class Problem21 {

    static int max = 10000;

    private static long d(long num) {
        long sum = 0;
        for (long divisor : Z.getDivisors(num, true)) {
            sum += divisor;
        }
        return sum;
    }

    private static long[] getPair(long a) {
        long b = d(a);
        if (b < max && a != b && d(b) == a)
            return new long[]{a, b};
        return null;
    }

    public static void main(String[] args) {
        long startTime = Z.now();
        ArrayList<long[]> pairList = new ArrayList<>();
        HashSet<Long> done = new HashSet<>();
        for (long i = 1; i < max; i++) {
            if (done.contains(i))
                continue;
            long[] pair = getPair(i);
            if (pair != null) {
                done.add(pair[0]);
                done.add(pair[1]);
                pairList.add(pair);
            }
        }

        long sum = 0;
        for (long[] pair : pairList) {
            for (long num : pair)
                sum += num;
        }

        Z.printAnswer(sum, startTime);
    }
}
