/**
 * program that finds the sum of all the multiples of 3 or 5 below 1000.
 */
public class problem1 {

    public static void main(String[] args) {
        int[] a;
        a = new int[1000];
        int i = 0, num = 1, sum = 0;

        while (num < 1000) {
            if (check(num)) {
                a[i] = num;
                i++;
            }
            num++;
        }

        for (int j = 0; j < i; j++) {
            sum = sum + a[j];
        }

        System.out.println("The sum is: " + sum);
        System.out.println("\nThere are " + i + " multiples of 3 and 5 below 1000.\n");
        System.out.println("This are all the multiples of 3 and 5 below 1000:");

        for (int j = 0; j < i; j++) {
            if (j + 1 == i) {
                System.out.print(a[j] + ".");
                break;
            }

            System.out.print(a[j] + ", ");
        }

    }

    public static boolean check(int num) {
        if (num % 3 == 0 || num % 5 == 0)
            return true;
        else
            return false;
    }

}
