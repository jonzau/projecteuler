import java.util.Date;

import static java.lang.Math.pow;

/**
 * A palindromic number reads the same both ways. The largest palindrome made from the product of two 2-digit numbers is 9009 = 91 × 99.
 * Find the largest palindrome made from the product of two 3-digit numbers.
 */
public class problem4 {

    public static void main(String[] args) {
        int n1 = 0;
        int n2 = 0;
        int ans = 0;
        int tmpNum;
        int tmp;
        int tmpDigits; //# of digits
        int tmpPow;

        int[] digits;

        int[] masterList = new int[1000 * 1000];
        String[] indexes = new String[1000 * 1000];
        int index = 0;

        long startTime = new Date().getTime();

        for (int i = 999; i > 99; i--) {
            for (int j = 999; j > 99; j--) {
                tmpNum = i * j;
                tmpDigits = (int) Math.floor(Math.log10(tmpNum)) + 1;
                digits = new int[tmpDigits];

                tmpPow = (int) pow(10, digits.length - 1);
                digits[0] = (tmpNum / tmpPow);
                tmp = digits[0] * tmpPow;
                for (int k = 1; k < digits.length; k++) {
                    tmpPow = (int) pow(10, (digits.length - k - 1));
                    digits[k] = (tmpNum - tmp) / tmpPow;
                    tmp = tmp + digits[k] * tmpPow;
                }

                if (digits[0] != digits[digits.length - 1]) {
                    continue;
                }

                if (isMirror(digits)) {
                    masterList[index] = tmpNum;
                    indexes[index] = "" + i + " * " + j + " = " + tmpNum;
                    index++;
//                    ans = tmpNum;
//                    n1 = i;
//                    n2 = j;
//                    break;
                }

                //print
                if (i % 100 == 0 && j == 999) {
                    System.out.println(i + " * " + j + " = " + tmp);
                    for (int k = 0; k < digits.length; k++) {
                        System.out.print(digits[k]);

                    }
                    System.out.println("");
                }
            }

        }

        index = getBigger(masterList, index);
        ans = masterList[index];
        long totalTime = (new Date().getTime()) - startTime;
        double timeSec = 1.0 * totalTime / 1000;

        System.out.println("Answer = " + ans);
        System.out.println("In " + timeSec + " sec");
        System.out.println(indexes[index]);
    }

    public static boolean isMirror(int[] digits) {

        for (int k = 0; k < digits.length / 2; k++) {
            if (digits[k] != digits[digits.length - 1 - k]) {
                return false;
            }

        }

        return true;
    }

    public static int getBigger(int[] masterList, int index) {
        int bigger = 0;
        int ind = 0;
        for (int i = 0; i < index; i++) {
            if (masterList[i] > bigger) {
                bigger = masterList[i];
                ind = i;
            }
        }
        return ind;
    }
}
