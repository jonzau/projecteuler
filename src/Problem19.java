import java.util.Date;

/**
 * 12/apr/2019
 * <p>
 * You are given the following information, but you may prefer to do some research for yourself.
 * <p>
 * 1 Jan 1900 was a Monday.
 * Thirty days has September,
 * April, June and November.
 * All the rest have thirty-one,
 * Saving February alone,
 * Which has twenty-eight, rain or shine.
 * And on leap years, twenty-nine.
 * A leap year occurs on any year evenly divisible by 4, but not on a century unless it is divisible by 400.
 * <p>
 * How many Sundays fell on the first of the month during the twentieth century (1 Jan 1901 to 31 Dec 2000)?
 */
public class Problem19 {

    public static void main(String[] args) {
        long startTime = Z.now();
        int count = 0;

        int sunday = 0;
        int jan = 0;
        int dec = 11;

        int day = sunday;

        int startYear = 1901;
        int startMonth = 1;
        int startDateDay = 1;

        int endYear = 2000;
        int endMonth = 12;
        int endDateDay = 31;

        Date startDate = new Date(0);
        startDate.setYear(startYear - 1900);
        startDate.setMonth(startMonth - 1);
        startDate.setDate(startDateDay);

        Date endDate = new Date(0);
        endDate.setYear(endYear - 1900);
        endDate.setMonth(endMonth - 1);
        endDate.setDate(endDateDay);

        while (startDate.getTime() <= endDate.getTime()) {
            if (startDate.getDay() == day)
                count++;
            if (startDate.getMonth() == dec) {
                startDate.setMonth(jan);
                startDate.setYear(startDate.getYear() + 1);
            } else {
                startDate.setMonth(startDate.getMonth() + 1);
            }
        }

        Z.printAnswer(count, startTime);
    }
}
