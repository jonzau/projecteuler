import java.math.BigInteger;

/**
 * 11/apr/2019
 * <p>
 * Power digit sum
 * 2^15 = 32768 and the sum of its digits is 3 + 2 + 7 + 6 + 8 = 26.
 * What is the sum of the digits of the number 2^1000?
 */
public class Problem16 {

    public static void main(String[] args) {
        long startTime = Z.now();
        String digits = new BigInteger("2").pow(1000).toString();
        long sum = 0;
        for (char digitChar : digits.toCharArray()) {
            int digit = Integer.parseInt("" + digitChar);
            sum += digit;
        }

        Z.printAnswer(sum, startTime);
    }
}
