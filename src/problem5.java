import java.util.Date;

/**
 * 2520 is the smallest number that can be divided by each of the numbers from 1 to 10 without any remainder.
 * What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
 */
public class problem5 {
    
    public static void main(String[] args) {
        int ans;
        boolean itIs;

        long startTime = new Date().getTime();

        for (int i = 20; i < 1000000000; i++) {
            itIs = true;
            for (int j = 1; j < 21; j++) {
                if (i % j != 0) {
                    itIs = false;
                    break;
                }
            }

            if (itIs) {
                ans = i;
                System.out.println("Answer = " + ans);
                long tt = new Date().getTime() - startTime;
                double t = 1.0 * tt / 1000;
                System.out.println("in " + t + " sec");
                return;
            }

        }
    }
}
